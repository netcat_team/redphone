/*
 * hw_uart.h
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#ifndef HW_UART_H_
#define HW_UART_H_

#define UART1_BAUDRATE 115200

void usart1_init(void);
void usart_out(USART_TypeDef *uart, uint8_t data);

#endif /* HW_UART_H_ */
