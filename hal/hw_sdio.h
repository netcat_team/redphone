/*
 * hw_sdio.h
 *
 *  Created on: Apr 9, 2017
 *      Author: netcat
 */

#ifndef HAL_HW_SDIO_H_
#define HAL_HW_SDIO_H_

void sdio_init();
void sdhc_read_block(uint8_t *buf, uint32_t addr);

typedef enum
{
 SDIO_RESP_NO = 0,
 SDIO_RESP_SORT = 0x40,
 SDIO_RESP_LONG = 0xC0,
 SDIO_RESP_R3 = 0xF40
} sdio_resp_t;

typedef enum
{
  SDIO_CMD0 = 0,
  SDIO_CMD8 = 8,
  SDIO_CMD55 = 55,
  SDIO_CMD41 = 41,
  SDIO_CMD2 = 2,
  SDIO_CMD3 = 3,
  SDIO_CMD9 = 9,
  SDIO_CMD7 = 7,
  SDIO_CMD6 = 6,
  SDIO_CMD24 = 24,
  SDIO_CMD25 = 25,
  SDIO_CMD12 = 12,
  SDIO_CMD13 = 13,
  SDIO_CMD17 = 17,
  SDIO_CMD18 = 18,
} sdio_cmd_t;

#endif /* HAL_HW_SDIO_H_ */
