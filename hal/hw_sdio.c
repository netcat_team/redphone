/*
 * hw_sdio.c
 *
 *  Created on: Apr 9, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>
//#include "stm32f429xx.h"
#include "hal/hw_gpio.h"
#include "hal/hw_sdio.h"
#include "spl/stm32f4xx_sdio.h"

void sdhc_read_block(uint8_t *buf, uint32_t addr)
{

}

gpio_init_t gpio_sdio_init[] = {
  { GPIOC,  8 }, // D0
  { GPIOC,  9 }, // D1
  { GPIOC, 10 }, // D2
  { GPIOC, 11 }, // D3
  { GPIOC, 12 }, // CLK
  { GPIOD,  2 }, // CMD
  {}};

static uint32_t sdio_cmd(uint32_t cmd, sdio_resp_t resp, uint32_t arg)
{
  //Clear the Command Flags
  SDIO->ICR = SDIO_STA_CCRCFAIL | SDIO_STA_CTIMEOUT | SDIO_STA_CMDREND | SDIO_STA_CMDSENT;
  SDIO->ARG = arg;
  SDIO->CMD = cmd | resp | SDIO_CMD_CPSMEN;

  uint32_t mask;
  if(resp == SDIO_RESP_NO) mask = SDIO_STA_CTIMEOUT | SDIO_STA_CMDSENT;
  else mask = SDIO_STA_CTIMEOUT | SDIO_STA_CMDREND | SDIO_FLAG_CCRCFAIL;

  uint32_t sta;
  while(1)
  {
    sta = SDIO->STA;
    if(sta & mask) break;
  }
//  if(sta & SDIO_STA_CTIMEOUT) slog("sdio: timeout");
//  else if(SDIO->STA & SDIO_FLAG_CCRCFAIL) slog("sdio: bad crc");

  return sta;
}

void sdio_init()
{
  RCC->AHB2ENR |= RCC_APB2ENR_SDIOEN;

  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN;
  gpio_init_af(gpio_sdio_init, GPIO_AF_SDIO);

  // init
  SDIO->CLKCR = 0;
  SDIO->POWER = 0x03;
}
