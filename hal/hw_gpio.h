/*
 * stm_gpio.h
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#ifndef HW_GPIO_H
#define HW_GPIO_H

#include "stm32f4xx.h"

typedef enum
{
  GPIO_MODE_IN = 0x00,
  GPIO_MODE_OUT = 0x01,
  GPIO_MODE_AF = 0x02,
  GPIO_MODE_AN = 0x03
} gpio_mode_t;

typedef enum
{
  GPIO_OTYPE_PP = 0x00,
  GPIO_OTYPE_OD = 0x01
} gpio_otype_t;

typedef enum
{
  GPIO_PUPD_NOPULL = 0x00,
  GPIO_PUPD_UP = 0x01,
  GPIO_PUPD_DOWN = 0x02
} gpio_pupd_t;

typedef enum
{
  GPIO_SPEED_2MHZ = 0x00,
  GPIO_SPEED_25MHZ = 0x01,
  GPIO_SPEED_50MHZ = 0x02,
  GPIO_SPEED_100MHZ = 0x03
} gpio_speed_t;

typedef enum
{
  GPIO_AF_RTC_50HZ = 0x00,
  GPIO_AF_MCO = 0x00,
  GPIO_AF_TAMPER = 0x00,
  GPIO_AF_SWJ = 0x00,
  GPIO_AF_TRACE = 0x00,
  GPIO_AF_TIM1 = 0x01,
  GPIO_AF_TIM2 = 0x01,
  GPIO_AF_TIM3 = 0x02,
  GPIO_AF_TIM4 = 0x02,
  GPIO_AF_TIM5 = 0x02,
  GPIO_AF_TIM8 = 0x03,
  GPIO_AF_TIM9 = 0x03,
  GPIO_AF_TIM10 = 0x03,
  GPIO_AF_TIM11 = 0x03,
  GPIO_AF_I2C1 = 0x04,
  GPIO_AF_I2C2 = 0x04,
  GPIO_AF_I2C3 = 0x04,
  GPIO_AF_SPI1 = 0x05,
  GPIO_AF_SPI2 = 0x05,
  GPIO_AF_SPI4 = 0x05,
  GPIO_AF_SPI5 = 0x05,
  GPIO_AF_SPI6 = 0x05,
  GPIO_AF_SPI3 = 0x06,
  GPIO_AF_SAI1 = 0x06,
  GPIO_AF_USART1 = 0x07,
  GPIO_AF_USART2 = 0x07,
  GPIO_AF_USART3 = 0x07,
  GPIO_AF_I2S3ext = 0x07,
  GPIO_AF_UART4 = 0x08,
  GPIO_AF_UART5 = 0x08,
  GPIO_AF_USART6 = 0x08,
  GPIO_AF_UART7 = 0x08,
  GPIO_AF_UART8 = 0x08,
  GPIO_AF_CAN1 = 0x09,
  GPIO_AF_CAN2 = 0x09,
  GPIO_AF_TIM12 = 0x09,
  GPIO_AF_TIM13 = 0x09,
  GPIO_AF_TIM14 = 0x09,
  GPIO_AF9_I2C2 = 0x09,
  GPIO_AF9_I2C3 = 0x09,
  GPIO_AF_OTG_FS = 0x0A,
  GPIO_AF_OTG_HS = 0x0A,
  GPIO_AF_ETH = 0x0B,
  GPIO_AF_FMC = 0x0C,
  GPIO_AF_OTG_HS_FS = 0x0C,
  GPIO_AF_SDIO = 0x0C,
  GPIO_AF_DCMI = 0x0D,
  GPIO_AF_LTDC = 0x0E,
  GPIO_AF_EVENTOUT = 0x0F,
} gpio_af_t;

typedef struct
{
  GPIO_TypeDef *GPIOx;
  uint32_t pin;
} gpio_init_t;

void gpio_init_ext(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode, gpio_otype_t otype, gpio_speed_t speed, gpio_pupd_t pupd);
void gpio_init_af(gpio_init_t *array, gpio_af_t af);
void gpio_init(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode);
void gpio_set_af(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_af_t af);


#endif
