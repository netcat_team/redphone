/*
 * stm_fmc.c
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>
#include "stm32f4xx.h"
#include "hal/hw_gpio.h"
#include "hal/hw_fmc.h"

gpio_init_t gpio_fmc_init[] = {
  // FMC
  { GPIOD, 0 }, // DQ2
  { GPIOD, 1 }, // DQ3
  { GPIOD, 8 }, // DQ13
  { GPIOD, 9 }, // DQ14
  { GPIOD, 10 }, // DQ15
  { GPIOD, 14 }, // DQ0
  { GPIOD, 15 }, // DQ1
  { GPIOE, 7 }, // DQ4
  { GPIOE, 8 }, // DQ5
  { GPIOE, 9 }, // DQ6
  { GPIOE, 10 }, // DQ7
  { GPIOE, 11 }, // DQ8
  { GPIOE, 12 }, // DQ9
  { GPIOE, 13 }, // DQ10
  { GPIOE, 14 }, // DQ11
  { GPIOE, 15 }, // DQ12
  { GPIOF, 0 }, // AQ0
  { GPIOF, 1 }, // AQ1
  { GPIOF, 2 }, // AQ2
  { GPIOF, 3 }, // AQ3
  { GPIOF, 4 }, // AQ4
  { GPIOF, 5 }, // AQ5
  { GPIOF, 12 }, // AQ6
  { GPIOF, 13 }, // AQ7
  { GPIOF, 14 }, // AQ8
  { GPIOF, 15 }, // AQ9
  { GPIOG, 0 }, // AQ10
  { GPIOG, 1 }, // AQ11
  { GPIOG, 2 }, // AQ12
  { GPIOG, 3 }, // AQ13
  // SDRAM
  { GPIOC, 0 }, // SDR_WE
  { GPIOC, 2 }, // SDR_E0
  { GPIOC, 3 }, // SDR_CKE0
  { GPIOE, 0 }, // SDR_NBL0
  { GPIOE, 1 }, // SDR_NBL1
  { GPIOF, 11 }, // SDR_RAS
  { GPIOG, 4 }, // SDR_BQ0
  { GPIOG, 5 }, // SDR_BQ1
  { GPIOG, 8 }, // SDR_CLK
  { GPIOG, 15 }, // SDR_CAS
  // LCD
  { GPIOD, 4 }, // LCD_OE
  { GPIOD, 5 }, // LCD_WE
  { GPIOD, 7 }, // LCD_E1
  {}};

void fmc_init()
{
  RCC->AHB3ENR |= RCC_AHB3ENR_FMCEN;

  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN |
                  RCC_AHB1ENR_GPIOFEN | RCC_AHB1ENR_GPIOGEN;
  gpio_init_af(gpio_fmc_init, GPIO_AF_FMC);
}

void fmc_sdram_init()
{
  // control
  FMC_Bank5_6->SDCR[0] =
  0x00000000 | // FMC_Bank1_SDRAM
  0x00000001 | // FMC_ColumnBits_Number_9b
  0x00000008 | // FMC_RowBits_Number_13b
  0x00000010 | // FMC_SDMemory_Width_16b
  0x00000040 | // FMC_InternalBank_Number_4
  0x00000180 | // FMC_CAS_Latency_3
  0x00000800 | // FMC_SDClock_Period_2
  0x00002000 ; // FMC_ReadPipe_Delay_1

  // timing
  FMC_Bank5_6->SDTR[0] =
    (2 - 1)         | // t_MRD = 2clk
    ((7 - 1) << 4)  | // t_XSR_min = 67ns
    ((4 - 1) << 8)  | // t_RAS_min = 37ns
    ((6 - 1) << 12) | // t_RC_min = 60ns
    ((2 - 1) << 16) | // t_WR = 14ns
    ((2 - 1) << 20) | // t_RP_min = 15ns
    ((2 - 1) << 24);  // t_RCD_min = 15ns

  // init
  while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);

  FMC_Bank5_6->SDCMR =
  0x00000001 | // FMC_Command_Mode_CLK_Enabled
  0x00000010;  // FMC_Command_Target_bank1
  uint32_t i;
  for(i = 0; i < 5000; i++) { asm volatile ("nop"); } // 10 ms
  while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);

  FMC_Bank5_6->SDCMR =
  0x00000002 | // FMC_Command_Mode_PALL
  0x00000010;  // FMC_Command_Target_bank1
  while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);

  FMC_Bank5_6->SDCMR =
  0x00000003 | // FMC_Command_Mode_AutoRefresh
  0x00000010 | // FMC_Command_Target_bank1
  ((4 - 1) << 5);
  while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);

  FMC_Bank5_6->SDCMR =
    0x00000004 | // FMC_Command_Mode_LoadMode
    0x00000010 | // FMC_Command_Target_bank1
    ((0x0001 | 0x0030 | 0x0200) << 9); // BURST_LENGTH_2, CAS_LATENCY_3, WRITEBURST_MODE_SINGLE

  // FMC_CLK = 90MHz
  // Refresh_Rate = 7.81us
  // Counter = (FMC_CLK * Refresh_Rate) - 20
  FMC_Bank5_6->SDRTR |= (683 << 1);

  while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
}

void fmc_sram_init()
{
  RCC->AHB1LPENR |= RCC_AHB1LPENR_SRAM1LPEN;

  FMC_Bank1->BTCR[0] =
    0x00000002 | // data addes mux enabled
    0x00000010 | // memory data width 16b
    0x00000100 | // burst access mode enabled
    0x00000400 | // wrap mode enabled
    0x00001000 | // write operation enabled
    0x00004000 | // extended erite timing mode enabled
    0x00100000;  // clock sync & async

  // address setup time 1
  FMC_Bank1->BTCR[1] = 1;
  FMC_Bank1E->BWTR[0] = 1; // 0x0FFFFFFF

  // enable
  FMC_Bank1->BTCR[0] |= 0x01;
}

