/*
 * hw_uart.c
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>
#include "stm32f429xx.h"
#include "hal/hw_uart.h"
#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"

const gpio_init_t gpio_uart_init[] = {
  // USART1
  { GPIOB, 6 }, // UART_EXT_TX
  { GPIOB, 7 }, // UART_EXT_RX
  {}};

static void usart_config(USART_TypeDef *uart)
{
  // cfg
  uart->CR1 = USART_CR1_TE | USART_CR1_RE;
  uart->CR2 = 0;
  uart->CR3 = 0;

  // baud
  uint32_t int_div, frac_div, t;
  int_div = (25 * RCC_APB1_CLK_HZ) / (2 * UART1_BAUDRATE);
  t = (int_div / 100) << 4;
  frac_div = int_div - 100 * (t >> 4);
  t |= ((frac_div * 16 + 50) / 100) & 0x0f;
  uart->BRR = t;

  // en
  uart->CR1 |= USART_CR1_UE;
}

void usart1_init()
{
  // gpio cfg
  RCC->APB1ENR |= RCC_AHB1ENR_GPIOBEN;
  gpio_init_af(gpio_uart_init, GPIO_AF_USART1);

  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

  usart_config(USART1);
}

void usart_out(USART_TypeDef *uart, uint8_t data)
{
  while(!(uart->SR & USART_SR_TXE));
  uart->DR = data;
}
