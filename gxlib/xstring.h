/*
 * xstring.h
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XSTRING_H_
#define GXLIB_XSTRING_H_

#include <string.h>
#include "xobject.h"

class XString
{
public:
  XString();
  XString(const XString& s);
  XString(const char *s);
  virtual ~XString();

  XString& operator=(const char *s);
  XString& operator=(const XString& s);
  const char *data() const;
  const bool isExist() const;
  int lenght() const { return mSize; }

private:
  char *p;
  uint32_t mAllocated;
  uint32_t mSize;

  char *alloc_mem(const char *s);
};

#endif /* GXLIB_XSTRING_H_ */
