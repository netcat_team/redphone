/*
 * xobject.h
 *
 *  Created on: Jan 4, 2016
 *      Author: netcat
 */

#ifndef XOBJECT_H_
#define XOBJECT_H_

#include "xlist.h"
#include "xsigslot.h"
#include <inttypes.h>

class XObject
{
public:
  XObject(XObject *obj = 0, bool isWidget = false);
  virtual ~XObject() {}

  XObject *parent() { return mParent; }
  void setParent(XObject *parent);
  bool isWidget() const { return mIsWidget; }
  XList<XObject*> *children() { return &mChildren; }

protected:
  bool mIsWidget;
  XObject *mParent;
  XList<XObject*> mChildren;
};

#endif /* XOBJECT_H_ */
