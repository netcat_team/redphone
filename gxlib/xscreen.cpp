/*
 * xscreen.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#include "xscreen.h"
#include "xsystem.h"

extern "C" {
#include "bsp/rm68120.h"
#include "painter.h"
}

XScreen::XScreen(XObject *obj = 0) : XObject(obj),
  mOr(SCR_PORTAIN), mSize(XSize(LCD_MAXX, LCD_MAXY)), mBuf(0)
{

}

void XScreen::push()
{
//  uint16_t *ptr = (uint16_t *)SDRAM_START_ADR;
//  for(uint32_t i = 0; i < LCD_SIZE; i++) LCD_WRITE_DATA(*ptr++);

  #define LCD_CPY_SIZE 0xfa00
  for(uint32_t i = 0; i < 6; i++) lcd_dma_ini_cp(SDRAM_START_ADR + (LCD_CPY_SIZE << 1) * i, LCD_CPY_SIZE);

//  painter_copy_layer_rect(SDRAM_START_ADR, SDRAM_START_ADR, 0, 0, LCD_MAXX, 800);
}
