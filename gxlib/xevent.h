/*
 * xevent.h
 *
 *  Created on: Jan 16, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XEVENT_H_
#define GXLIB_XEVENT_H_

class XEvent
{
public:
  XEvent() : mAccepted(false) { }
  bool isAccepted() const { return mAccepted; }
  void accept() { mAccepted = true; }

private:
  bool mAccepted;
};

typedef enum
{
  TS_STATUS_PRESSED,
  TS_STATUS_MOVED,
  TS_STATUS_RELEASED,
} ts_status_e;

class XTouchEvent : public XEvent
{
  friend class XSystem;
  friend class XTouch;

public:
  XTouchEvent() : XEvent(), mStatus(TS_STATUS_PRESSED) { }
  const XPoint& point() const { return mPoint; }
  const ts_status_e& status() const { return mStatus; }

private:
  XPoint mPoint;
  XPoint mGlobal;
  ts_status_e mStatus;
};

#endif /* GXLIB_XEVENT_H_ */
