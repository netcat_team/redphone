#ifndef XPOINT_H
#define XPOINT_H

#include "xobject.h"

template <class T>
class TPoint
{
    public:
        TPoint(T x = 0, T y = 0) : mX(x), mY(y) { }

        // get
        T x() const { return mX; }
        T y() const { return mY; }
        T &rx() { return mX; }
        T &ry() { return mY; }

        // set
        void setX(T x) { mX = x; }
        void setY(T y) { mY = y; }
        void move(T x, T y) { mX = x; mY = y; }

        bool operator==(const TPoint<T> &obj) const { return mX == obj.mX && mY == obj.mY; }
        bool operator!=(const TPoint<T> &obj) const { return mX != obj.mX || mY != obj.mY; }

        void operator+=(const TPoint<T> &obj) { mX += obj.mX; mY += obj.mY; }
        void operator-=(const TPoint<T> &obj) { mX -= obj.mX; mY -= obj.mY; }

//        static T vm(const TPoint<T> &a, const TPoint<T> &b, const TPoint<T> &c)
//        { return a.x() * b.y() - a.x() * c.y() - b.x() * a.y() + b.x() * c.y() + c.x() * a.y() - c.x() * b.y(); }

//        static T sm(const TPoint<T> &a, const TPoint<T> &b, const TPoint<T> &c)
//        { return (c.x() - b.x()) * (a.x() - b.x()) + (c.y() - b.y()) * (a.y() - b.y()); }

    private:
        T mX, mY;
};

typedef TPoint<int32_t> XPoint;
typedef TPoint<float> XPointFloat;

inline XPoint operator+(const XPoint &obj1, const XPoint &obj2) { return XPoint(obj1.x() + obj2.x(), obj1.y() + obj2.y()); }
inline XPoint operator-(const XPoint &obj1, const XPoint &obj2) { return XPoint(obj1.x() - obj2.x(), obj1.y() - obj2.y()); }

#endif // XPOINT_H
