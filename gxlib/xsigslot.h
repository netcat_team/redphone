#ifndef XSIGNAL_H
#define XSIGNAL_H

template<class... Types>
class XSlotBase
{
    public:
        virtual ~XSlotBase() {}
        virtual void run(Types... t...) = 0;
};

template<class T, class... Types>
class XSlot : public XSlotBase<Types...>
{
    public:
        typedef void (T::*MemberFunPtr)(Types...);

        XSlot(T* objPtr, MemberFunPtr funPtr)
            : mObjPtr(objPtr), mFunPtr(funPtr) { }

        void run(Types...)
        {
            mFunPtr(mObjPtr);
        }

    private:
        T* mObjPtr;
        MemberFunPtr mFunPtr;
};

template<class... Types>
class XSignal
{
    public:
        XSignal() { mSlots.clear(); mSignals.clear(); }
        ~XSignal() { mSlots.clear(); mSignals.clear(); }

        template<class T>
        void connect(const XSlot<T, Types...>& slot) { mSlots.insert(&slot); }
        void connect(const XSignal<Types...>& signal) { mSignals.insert(&signal); }

        template<class T>
        void disconnect(const XSlot<T, Types...>& slot) { mSlots.erase(&slot); }
        void disconnect(const XSignal<Types...>& signal) { mSignals.erase(&signal); }

        void operator()(Types... )
        {
          XSlotBase<Types...> *sl = mSlots.first();
          while(sl)
          {
            sl->run();
            sl = mSlots.next();
          }

          XSignal<Types...> *s = mSignals.first();
          while(s)
          {
            s->operator()();
            s = mSignals.next();
          }
        }

        void disconnectAll() { mSignals.clear(); mSlots.clear(); }
        void disconnectSignals() { mSignals.clear(); }
        void disconnectSlots() { mSlots.clear(); }

    private:
        XList<XSlotBase<Types...> *> mSlots;
        XList<XSignal<Types...> *> mSignals;

};

#endif // XSIGNAL_H
