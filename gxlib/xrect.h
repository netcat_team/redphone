#ifndef XRECT_H
#define XRECT_H

#include "xpoint.h"
#include "xsize.h"

#define MIN(a, b) ((a < b) ? (a) : (b))
#define MAX(a, b) ((a > b) ? (a) : (b))
template<typename T>
inline void xSwap(T& a, T& b) { T tmp = a; a = b; b = tmp; }

template <class T>
class TRect
{
    public:
        TRect() : mPos(TPoint<T>()), mSize(TSize<T>()) {}
        TRect(T x, T y, T w, T h) : mPos(TPoint<T>(x, y)), mSize(TSize<T>(w, h)) {}

        TRect(const TPoint<T> &p1, const TPoint<T> &p2)
        {
            T x1, x2, y1, y2;
            x1 = p1.x();
            y1 = p1.y();
            x2 = p2.x();
            y2 = p2.y();
            if(x2 < x1) xSwap(x1, x2);
            if(y2 < y1) xSwap(y1, y2);
            move(x1, y1);
            resize(x2 - x1, y2 - y1);
        }

        TRect(const TPoint<T> &pos, const TSize<T> &size)
            : mPos(pos), mSize(size) {}

        TRect(const TSize<T> &size)
            : mPos(TPoint<T>()), mSize(size) {}

        TRect(const TRect<T> &rect) : mPos(rect.pos()), mSize(rect.size()) {}

        bool isExist() const { return mSize.isExist(); }

        // get
        T left() const { return mPos.x(); }
        T top() const { return mPos.y(); }
        T bottom() const { return mPos.y() + mSize.h(); }
        T right() const { return mPos.x() + mSize.w(); }
        TPoint<T> topRight() const { return TPoint<T>(right(), top()); }
        TPoint<T> topLeft() const { return TPoint<T>(left(), top()); }
        TPoint<T> bottomLeft() const { return TPoint<T>(left(), bottom()); }
        TPoint<T> bottomRight() const { return TPoint<T>(right(), bottom()); }

        TRect<T> operator&(const TRect<T> &r) const
        {
            if(!intersects(r)) return TRect<T>();
            return TRect<T>(TPoint<T>(MAX(left(), r.left()), MAX(top(), r.top())),
                            TPoint<T>(MIN(right(), r.right()), MIN(bottom(), r.bottom())));
        }
        TRect<T>& operator&=(const TRect<T> &r)
        {
            *this = *this & r;
            return *this;
        }
        TRect<T> intersected(const TRect<T> &rect) const { return *this & rect; }

        bool intersects(const TRect<T> &r) const
        {
            return (MAX(left(), r.left()) <= MIN(right(), r.right()) &&
                    MAX(top(), r.top()) <= MIN(bottom(), r.bottom()));
        }

        bool operator==(const TRect<T> &r)
            { return r.pos() == pos() && r.size() == size(); }
        bool operator!=(const TRect<T> &r)
            { return r.pos() != pos() || r.size() != size(); }

        T width() const { return mSize.w(); }
        T height() const { return mSize.h(); }

        const TSize<T> &size() const { return mSize; }
        const TPoint<T> &pos() const { return mPos; }

        // set
        void setX(const T x) { mPos.setX(x); }
        void setY(const T y) { mPos.setY(y); }

        void move(const T x, const T y) { mPos.move(x, y); }
        void move(const TPoint<T> &pos) { mPos = pos; }

        void resize(const T w, const T h) { mSize = TSize<T>(w, h); }
        void resize(const TSize<T> &size) { mSize = size; }

        void translate(const TPoint<T> &pos) { mPos += pos; }

        bool inside(const T x, const T y) const
        { return x >= left() && x < right() && y >= top() && y < bottom(); }

        bool inside(const TPoint<T> &pos) const { return inside(pos.x(), pos.y()); }

        bool isSquare() const { return mSize.w() == mSize.h(); }

        TPoint<T> center() const { return TPoint<T>(mSize.w() / 2, mSize.h() / 2); }

    private:
        TPoint<T> mPos;
        TSize<T> mSize;
};

typedef TRect<int32_t> XRect;
typedef TRect<float> XRectF;

#endif // XRECT_H
