/*
 * xstring.cpp
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#include "xstring.h"

XString::XString()
: p(alloc_mem(""))
{}

XString::XString(const XString& s) :
  p(0)
{
  mSize = s.mSize;
  mAllocated = s.mAllocated;
  p = new char[mSize];
  memcpy(p, s.p, mAllocated);
}

XString::XString(const char *s)
: p(alloc_mem(s))
{}

XString::~XString()
{
  delete[] p;
}

XString& XString::operator=(const char *s)
{
  if(p != s)
  {
    delete[] p;
    alloc_mem(s);
  }
  return *this;
}

XString& XString::operator=(const XString& s)
{
    return operator=(s.p);
}

char *XString::alloc_mem(const char *s)
{
  mSize = strlen(s);
  mAllocated = mSize + 1;
  char *p2 = new char[mAllocated];
  memcpy(p2, s, mAllocated);

  return p2;
}

const char *XString::data() const
{
  return p;
}

const bool XString::isExist() const
{
  return mSize > 0;
}
