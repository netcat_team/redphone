/*
 * xtouch.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#include "xtouch.h"
#include "xsystem.h"

extern "C" {
#include "ub_lib/stm32_ub_touch_stmpe811.h"
}

XSystem *mSys;

XTouch::XTouch(XObject *obj = 0) : XObject(obj), mLastActiveFlag(false)
{
  mSys = xSys();

}

XTouch::~XTouch()
{

}

void XTouch::update()
{
  XTouchEvent e;
  Touch_Data_t *t = ts_get_data();
  t->status = TOUCH_RELEASED;

  ts_update();

  if(t->status == TOUCH_PRESSED)
  {
    if(mLastActiveFlag)
    {
      e.mGlobal.move(t->xp, t->yp);
      e.mStatus = TS_STATUS_MOVED;
    }
    else
    {
      mLastActiveFlag = true;

      e.mGlobal.move(t->xp, t->yp);
      e.mStatus = TS_STATUS_PRESSED;
    }

    xSys()->deliverTouch(&e);
  }
  else
  {
    if(mLastActiveFlag)
    {
      mLastActiveFlag = false;

      e.mGlobal.move(t->xp, t->yp);
      e.mStatus = TS_STATUS_RELEASED;

      xSys()->deliverTouch(&e);
    }
  }
}
