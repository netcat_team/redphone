/*
 * xobject.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xobject.h"
#include "xsystem.h"

XObject::XObject(XObject *obj, bool isWidget) : mIsWidget(isWidget), mParent(obj)
{
  if(mParent) mParent->children()->add(this);
}

void XObject::setParent(XObject *parent)
{
  if(mParent) mParent->children()->remove(this);
  mParent = parent;
  parent->children()->add(this);

  if(parent->isWidget()) xSys()->redraw();
}
