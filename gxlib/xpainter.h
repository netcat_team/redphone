/*
 * xpainter.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XPAINTER_H_
#define GXLIB_XPAINTER_H_

#include "xpainter.h"
#include "xobject.h"
#include "xrect.h"
#include "xstring.h"

#define xRgba(r, g, b, a) ((uint32_t)(((a) << 24) | ((r) << 16) | ((g) << 8) | (b)))
#define xRgb(r, g, b) xRgba(r, g, b, 255)

typedef enum
{
  AlignLeft = 0x01,
  AlignRight = 0x02,
  AlignHCenter = 0x04,

  AlignTop = 0x10,
  AlignBottom = 0x20,
  AlignVCenter = 0x04,

  AlignCenter = AlignHCenter | AlignVCenter
} AlignmentFlag;

class XPainter: public XObject
{
public:
  XPainter(XObject *obj = 0, uint8_t *buf = 0);
  virtual ~XPainter() {}

  void setBuf(uint8_t *buf) { mBuf = buf; }

  void drawRect(const XRect& rect);
  void fillRect(const XRect& rect);

  void drawEllips(const XRect& rect);

  void drawText(const XPoint& pos, int flag, const XString& text);
  void drawText(const XPoint& pos, const XString& text) { drawText(pos, 0, text); }

//  void drawLine(const XPoint& p1, const XPoint& p2);
//  void drawPixmap(const XPoint& p1, const char *data);

  void setWindow(const XRect& rect);
  void shear(const XPoint& pos);

  void setPen(uint32_t color);
  void setBrush(uint32_t color);

  void clear();
  void reset();

private:
  uint8_t *mBuf;
  uint32_t mPen;
  uint32_t mBrush;
};

#endif /* GXLIB_XPAINTER_H_ */
