/*
 * xpainter.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xpainter.h"
extern "C" {
#include "painter.h"
#include "fonts.h"
}

XPainter::XPainter(XObject *obj, uint8_t *buf) : XObject(obj),
  mBuf(buf), mPen(xRgb(0, 0, 0)), mBrush(xRgba(0, 0, 0, 0))
{

}

void XPainter::drawRect(const XRect& rect)
{
  painter_set_color(mPen);
  painter_draw_rect(rect.pos().x(), rect.pos().y(), rect.size().w(), rect.size().h());
  painter_set_color(mBrush);
  painter_fill_rect(rect.pos().x() + 1, rect.pos().y() + 1, rect.size().w() - 1, rect.size().h() - 1);
}

void XPainter::drawEllips(const XRect& rect)
{
  if(rect.isSquare())
  {
    XPoint p = XPoint((rect.width() >> 1) + rect.pos().x(), (rect.height() >> 1) + rect.pos().y());
    painter_set_color(mBrush);
    painter_fill_circle(p.x(), p.y(), rect.width() >> 1);
    painter_set_color(mPen);
    painter_draw_circle(p.x(), p.y(), rect.width() >> 1);
  }
  else
  {
    drawRect(rect);
  }
}

void XPainter::fillRect(const XRect& rect)
{
  painter_fill_rect(rect.pos().x(), rect.pos().y(), rect.size().w(), rect.size().h());
}

void XPainter::setWindow(const XRect& rect)
{
  painter_window(rect.pos().x(), rect.pos().y(), rect.size().w(), rect.size().h());
}

void XPainter::shear(const XPoint& pos)
{
  painter_offset_scr(pos.x(), pos.y());
}

void XPainter::setPen(uint32_t color)
{
  mPen = color;
}

void XPainter::setBrush(uint32_t color)
{
  mBrush = color;
}

void XPainter::reset()
{
  mPen = xRgb(0, 0, 0);
  mBrush = xRgba(0, 0, 0, 0);
}

void XPainter::clear()
{
  painter_clear();
}

void XPainter::drawText(const XPoint& pos, int flag, const XString& text)
{
  int x, y;

  if(flag & AlignHCenter) x = (text.lenght() * 6) >> 1;
  else x = 0;

  if(flag & AlignVCenter) y = 4;
  else y = 0;

  if(!text.isExist()) return;
  const char *str = text.data();
  uint8_t i, j, k;
  char s;

  painter_set_color(mPen);

  for(k = 0; *str; k++)
  {
      s = *str++;
      if(s > 0x7a || s < 0x20) s = ' ';
      s -= 0x20;

      for(j = 0; j < 8; j++)
          for(i = 0; i < 5; i++)
              if(font_raster5x8[s * 5 + i] & (1 << j)) painter_point(pos.x() + k * 6 + i - x, pos.y() + j - y);
  }
}
