/*
 * xsystem.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XSYSTEM_H_
#define GXLIB_XSYSTEM_H_

#include "xobject.h"
#include "xwidget.h"
#include "xscreen.h"
#include "xtouch.h"

class XSystem : public XObject
{
  friend class XTouch;

public:
  static XSystem *instance()
  {
    if(!mSelf)
    {
      mSelf = new XSystem();
      mSelf->init();
    }
    return mSelf;
  }

  void redraw();
  XWidget *topLevelWidget() { return mTopLevelWidget; }
  void syslog(const char *msg) { }

  XTouch *mTouch;

private:
  static XSystem *mSelf;
  XScreen *mScreen;
  XPainter *mPainter;
  XWidget *mTopLevelWidget;
  XWidget *mLastFocus;


  XSystem() : mTouch(0), mScreen(0), mPainter(0), mTopLevelWidget(0), mLastFocus(0) {}
  void init();
  bool paintWidget(XObject *obj);
  void goTreeReverse(XObject *w, bool (XSystem::*action)(XObject *obj));
  void deliverTouch(XTouchEvent *e);
  void goTreeForward(XTouchEvent *e, XObject *w, bool (XSystem::*action)(XTouchEvent *e, XObject *obj), bool (XSystem::*action2)(XTouchEvent *e, XObject *obj));
  bool touchWidget(XTouchEvent *e, XObject *obj);
  bool touchIf(XTouchEvent *e, XObject *obj);
};

inline XSystem *xSys() { return XSystem::instance(); }

#endif /* GXLIB_XSYSTEM_H_ */
