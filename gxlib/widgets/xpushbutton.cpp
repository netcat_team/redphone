/*
 * xpushbutton.cpp
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#include "xpushbutton.h"
#include "xsystem.h"

XPushButton::XPushButton(XWidget *parent) : XAbstractButton(parent)
{}

XPushButton::~XPushButton()
{}

void XPushButton::paintEvent(XPainter *p)
{
  p->setPen(xRgb(50, 120, 210));

  if(mChecked)
  {
    p->setBrush(xRgba(255, 255, 255, 0));
    p->drawRect(XRect(0, 0, size().w() - 2, size().h() - 2));

    p->setBrush(xRgb(100, 100, 150));
    p->drawRect(XRect(2, 2, size().w() - 6, size().h() - 6));
  }
  else
  {
    p->setBrush(xRgb(80, 80, 100));
    p->drawRect(XRect(0, 0, size().w() - 2, size().h() - 2));
  }

  p->setPen(xRgb(255, 255, 255));
  p->drawText(geometry().center(), AlignCenter, mText);
}

void XPushButton::tsEvent(XTouchEvent *e)
{
  e->accept();

  switch(e->status())
  {
    case TS_STATUS_PRESSED:
      mChecked = true;
      update();
      break;

    case TS_STATUS_MOVED:
      break;

    case TS_STATUS_RELEASED:
      if(XRect(size()).inside(e->point())) sReleased();
      mChecked = false;
      update();
      break;
  }
}
