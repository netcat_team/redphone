#ifndef XWINDOW_H
#define XWINDOW_H

#include "xwidget.h"
#include "xlabel.h"

class XWindow : public XWidget
{
public:
    XWindow(XWidget *parent = 0);

    void setWindowTitle(const XString& text);

protected:
    virtual void paintEvent(XPainter *p);
    virtual void tsEvent(XTouchEvent *e);

private:
    bool mMove;
    XPoint mTouchPoint;
    XString mTitle;
};

#endif // XWINDOW_H
