/*
 * xlabel..cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xlabel.h"
#include "xsystem.h"

XLabel::XLabel(XObject *parent) : XWidget(parent)
{

}

void XLabel::paintEvent(XPainter *p)
{
  p->setPen(xRgb(255, 255, 255));
  p->drawText(XPoint(0, (size().h() - 8) >> 1), mText);
}

void XLabel::setText(const XString& text)
{
  mText = text;
  update();
}
