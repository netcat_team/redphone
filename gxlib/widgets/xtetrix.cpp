/*
 * xtetrix.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: netcat
 */

#include "xtetrix.h"

#define TETRIX_X 10
#define TETRIX_Y 16

uint32_t mem[TETRIX_X * TETRIX_Y] =
{
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  2, 2, 3, 3, 1, 1, 0, 0, 0, 0,
  2, 1, 1, 3, 1, 1, 7, 4, 4, 4,
  2, 1, 1, 3, 6, 0, 7, 0, 4, 2,
  0, 4, 5, 5, 6, 6, 7, 0, 0, 2,
  4, 4, 4, 5, 5, 6, 7, 0, 2, 2,
};

const uint32_t fcolors[] = {
        xRgba(255, 255, 255, 5),
        xRgba(255, 100, 75, 200), // red
        xRgba(220, 5, 255, 200), // purple
        xRgba(100, 100, 255, 200), // dark blue
        xRgba(255, 255, 0, 200), // yellow
        xRgba(50, 200, 25, 200), // green
        xRgba(0, 220, 255, 200), // blue
        xRgba(255, 150, 5, 200) // orange
};

const uint32_t bcolors[] = {
        xRgba(255, 255, 255, 5),
        xRgba(230, 55, 40, 200), // red
        xRgba(155, 0, 170, 200), // purple
        xRgba(55, 55, 250, 200), // dark blue
        xRgba(230, 225, 0, 200), // yellow
        xRgba(15, 125, 0, 200), // green
        xRgba(0, 150, 180, 200), // blue
        xRgba(220, 130, 5, 200) // orange
};

XTetrix::XTetrix(XWidget *parent)
: XWidget(parent)
{

}

XTetrix::~XTetrix()
{

}

void XTetrix::paintEvent(XPainter *p)
{
  uint32_t x, y, inc;

  p->setPen(xRgb(50, 160, 210));
  p->setBrush(xRgba(0, 0, 0, 100));
  p->drawRect(XRect(0, 0, size().w() - 1, size().h() - 1));

  inc = 0;
  for(y = 0; y < TETRIX_Y; y++)
    for(x = 0; x < TETRIX_X; x++)
    {
      p->setPen(fcolors[mem[inc]]);
      p->setBrush(bcolors[mem[inc]]);
      p->drawRect(XRect(2 + x * 12, 2 + y * 12, 10, 10));
      inc++;
    }
}
