/*
 * xslider.cpp
 *
 *  Created on: Feb 11, 2016
 *      Author: netcat
 */

#include "xslider.h"

XSlider::XSlider(XWidget *parent) : XWidget(parent),
  mMin(0), mMax(100), mPos(SLIDER_RADIUS)
{
}

XSlider::~XSlider()
{
}

void XSlider::updateRange()
{
  mMath = ((float)(mMax - mMin) / (float)(size().w() - SLIDER_RADIUS * 2));
}

void XSlider::setValue(int32_t value)
{
  value = MAX(MIN(mMin, value), mMax);
  mPos = (uint32_t)(value / mMath) + SLIDER_RADIUS;
  update();
}

int32_t XSlider::value()
{
  return mMin + (int32_t)(mMath * (mPos - SLIDER_RADIUS));
}

void XSlider::setRange(int32_t min, int32_t max)
{
  mMin = min;
  mMax = max;
  updateRange();
}

void XSlider::tsEvent(XTouchEvent *e)
{
  e->accept();

  switch(e->status())
  {
    case TS_STATUS_PRESSED:
    case TS_STATUS_MOVED:
    {
      int32_t click = e->point().x();

      if(click < SLIDER_RADIUS) click = SLIDER_RADIUS;
      else if(click > size().w() - SLIDER_RADIUS) click = size().w() - SLIDER_RADIUS;
      mPos = click;

      update();
    }
    break;

    case TS_STATUS_RELEASED:
      break;
  }
}

void XSlider::paintEvent(XPainter *p)
{
  uint32_t h = (size().h() >> 1) - 2;

  p->setPen(xRgb(150, 150, 150));
  p->setBrush(xRgb(80, 80, 80));
  p->drawRect(XRect(SLIDER_RADIUS, h, size().w() - (SLIDER_RADIUS << 1), 4));

  p->setPen(xRgb(50, 100, 250));
  p->setBrush(xRgb(20, 80, 130));
  p->drawEllips(XRect(mPos - SLIDER_RADIUS, h + 2 - SLIDER_RADIUS, SLIDER_RADIUS * 2, SLIDER_RADIUS * 2));
}

void XSlider::resizeEvent()
{
  updateRange();
}
