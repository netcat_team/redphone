/*
 * xtetrix.h
 *
 *  Created on: Feb 4, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XTETRIX_H_
#define GXLIB_WIDGETS_XTETRIX_H_

#include "xwidget.h"

class XTetrix: public XWidget
{
public:
  XTetrix(XWidget *parent = 0);
  virtual ~XTetrix();

protected:
  virtual void paintEvent(XPainter *p);
};

#endif /* GXLIB_WIDGETS_XTETRIX_H_ */
