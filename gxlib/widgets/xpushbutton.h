/*
 * xpushbutton.h
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XPUSHBUTTON_H_
#define GXLIB_WIDGETS_XPUSHBUTTON_H_

#include "xabstractbutton.h"

class XPushButton: public XAbstractButton
{
public:
  XPushButton(XWidget *parent = 0);
  virtual ~XPushButton();

protected:
  virtual void paintEvent(XPainter *p);
  virtual void tsEvent(XTouchEvent *e);
};

#endif /* GXLIB_WIDGETS_XPUSHBUTTON_H_ */
