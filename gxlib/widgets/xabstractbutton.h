/*
 * xabstractbutton.h
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XABSTRACTBUTTON_H_
#define GXLIB_WIDGETS_XABSTRACTBUTTON_H_

#include "xwidget.h"

class XAbstractButton : public XWidget
{
public:
  XAbstractButton(XWidget *parent = 0) : XWidget(parent), mChecked(false), mCheckable(false) {}
  virtual ~XAbstractButton() {}

  bool isCheckable() { return mCheckable; }
  bool isChecked() { return mChecked; }
  void setChecked(bool state = true) { if(mCheckable) { mChecked = state; update(); } }

  void setText(const XString& text) { mText = text; update(); }
  const XString& text() { return mText; }

  XSignal<> sReleased;

protected:
  bool mChecked, mCheckable;
  XString mText;
};

#endif /* GXLIB_WIDGETS_XABSTRACTBUTTON_H_ */
