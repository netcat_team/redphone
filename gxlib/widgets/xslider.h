/*
 * xslider.h
 *
 *  Created on: Feb 11, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XSLIDER_H_
#define GXLIB_WIDGETS_XSLIDER_H_

#include "xwidget.h"

#define SLIDER_RADIUS 7

class XSlider: public XWidget
{
public:
  XSlider(XWidget *parent);
  virtual ~XSlider();
  void setRange(int32_t min, int32_t max);

  void setValue(int32_t value);
  int32_t value();

protected:
  virtual void paintEvent(XPainter *p);
  virtual void tsEvent(XTouchEvent *e);
  virtual void resizeEvent();

private:
  int32_t mMin, mMax;
  uint32_t mPos;
  float mMath;

  void updateRange();
};

#endif /* GXLIB_WIDGETS_XSLIDER_H_ */
