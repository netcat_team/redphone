/*
 * xlabel.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XLABEL_H_
#define GXLIB_WIDGETS_XLABEL_H_

#include "xwidget.h"
#include "xsigslot.h"

class XLabel: public XWidget
{
public:
  XLabel(XObject *parent = 0);
  virtual ~XLabel() {};

  void setText(const XString& text);

  XSignal<> sClicked;

protected:
  XString mText;

  virtual void paintEvent(XPainter *p);
};

#endif /* GXLIB_WIDGETS_XLABEL_H_ */
