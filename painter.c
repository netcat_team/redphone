/*
 * painter.c
 *
 *  Created on: Jan 4, 2016
 *      Author: netcat
 */
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32_ub_touch_stmpe811.h"
#include "bsp/rm68120.h"

#include "stm32f4xx_dma.h"
#include "stm32f4xx_dma2d.h"

#include "painter.h"

#include <stdlib.h>

uint32_t vbuf = LCD_FRAME_BUFFER;
uint32_t vbuf_clut = LCD_FRAME_BUFFER + LCD_FRAME_OFFSET;

int32_t vbuf_minx = 0;
int32_t vbuf_miny = 0;
int32_t vbuf_maxx = 0;
int32_t vbuf_maxy = 0;

int32_t sy = 0;
int32_t sx = 0;

void painter_window(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  vbuf_minx = x;
  vbuf_miny = y;
  vbuf_maxx = vbuf_minx + w;
  vbuf_maxy = vbuf_miny + h;
}

void painter_offset_scr(int32_t x, int32_t y)
{
  sx = x;
  sy = y;
}

void painter_init()
{
  painter_clear();
}

void painter_fast_fill_rect_dma(uint32_t vbuf_to, uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t c)
{
  DMA2D_InitTypeDef DMA2D_InitStruct;

  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  DMA2D_DeInit();
  DMA2D_StructInit(&DMA2D_InitStruct);
  DMA2D_InitStruct.DMA2D_Mode = DMA2D_R2M;
  DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
  DMA2D_InitStruct.DMA2D_OutputGreen = (c >> 8) & 0xff;
  DMA2D_InitStruct.DMA2D_OutputBlue = (c >> 0) & 0xff;
  DMA2D_InitStruct.DMA2D_OutputRed = (c >> 16) & 0xff;
  DMA2D_InitStruct.DMA2D_OutputAlpha = (c >> 24) & 0xff;
  DMA2D_InitStruct.DMA2D_OutputMemoryAdd = vbuf_to + offset;
  DMA2D_InitStruct.DMA2D_OutputOffset = (LCD_MAXX - w);
  DMA2D_InitStruct.DMA2D_NumberOfLine = h;
  DMA2D_InitStruct.DMA2D_PixelPerLine = w;
  DMA2D_Init(&DMA2D_InitStruct);

  DMA2D_StartTransfer();
  while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET);
}

void painter_copy_layer_rect(uint32_t vbuf_to, uint32_t vbuf_from, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  DMA2D_InitTypeDef DMA2D_InitStruct;
  DMA2D_DeInit();
  DMA2D_StructInit(&DMA2D_InitStruct);
  DMA2D_InitStruct.DMA2D_Mode = DMA2D_M2M;
  DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
  DMA2D_InitStruct.DMA2D_OutputMemoryAdd = vbuf_to;
  DMA2D_InitStruct.DMA2D_OutputGreen = 0;
  DMA2D_InitStruct.DMA2D_OutputBlue = 0;
  DMA2D_InitStruct.DMA2D_OutputRed = 0;
  DMA2D_InitStruct.DMA2D_OutputAlpha = 0;
  DMA2D_InitStruct.DMA2D_OutputOffset = LCD_MAXX - w;
  DMA2D_InitStruct.DMA2D_NumberOfLine = h;
  DMA2D_InitStruct.DMA2D_PixelPerLine = w;
  DMA2D_Init(&DMA2D_InitStruct);

  DMA2D_FG_InitTypeDef DMA2D_FG_InitStruct;
  DMA2D_FG_StructInit(&DMA2D_FG_InitStruct);
  DMA2D_FG_InitStruct.DMA2D_FGMA = vbuf_from;
  DMA2D_FG_InitStruct.DMA2D_FGCM = CM_RGB565;
  DMA2D_FG_InitStruct.DMA2D_FGPFC_ALPHA_MODE = REPLACE_ALPHA_VALUE;
  DMA2D_FG_InitStruct.DMA2D_FGO = LCD_MAXX - w;
  DMA2D_FGConfig(&DMA2D_FG_InitStruct);

  DMA2D_BG_InitTypeDef DMA2D_BG_InitStruct;
  DMA2D_BG_StructInit(&DMA2D_BG_InitStruct);
  DMA2D_BG_InitStruct.DMA2D_BGMA = vbuf_to;
  DMA2D_BG_InitStruct.DMA2D_BGCM = CM_RGB565;
  DMA2D_BG_InitStruct.DMA2D_BGPFC_ALPHA_MODE = REPLACE_ALPHA_VALUE;
  DMA2D_BG_InitStruct.DMA2D_BGO = LCD_MAXX - w;
  DMA2D_BGConfig(&DMA2D_BG_InitStruct);

  DMA2D_StartTransfer();

  while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET);
}


void painter_fill_rect_dma_clut(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  DMA2D_InitTypeDef DMA2D_InitStruct;
  DMA2D_DeInit();
  DMA2D_StructInit(&DMA2D_InitStruct);
  DMA2D_InitStruct.DMA2D_Mode = DMA2D_M2M_BLEND;
  DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
  DMA2D_InitStruct.DMA2D_OutputMemoryAdd = vbuf + offset;
  DMA2D_InitStruct.DMA2D_OutputGreen = 0;
  DMA2D_InitStruct.DMA2D_OutputBlue = 0;
  DMA2D_InitStruct.DMA2D_OutputRed = 0;
  DMA2D_InitStruct.DMA2D_OutputAlpha = 0;
  DMA2D_InitStruct.DMA2D_OutputOffset = LCD_MAXX - w;
  DMA2D_InitStruct.DMA2D_NumberOfLine = h;
  DMA2D_InitStruct.DMA2D_PixelPerLine = w;
  DMA2D_Init(&DMA2D_InitStruct);

  DMA2D_FG_InitTypeDef DMA2D_FG_InitStruct;
  DMA2D_FG_StructInit(&DMA2D_FG_InitStruct);
  DMA2D_FG_InitStruct.DMA2D_FGMA = vbuf_clut;
  DMA2D_FG_InitStruct.DMA2D_FGCM = CM_L8;
  DMA2D_FGConfig(&DMA2D_FG_InitStruct);

  DMA2D_BG_InitTypeDef DMA2D_BG_InitStruct;
  DMA2D_BG_StructInit(&DMA2D_BG_InitStruct);
  DMA2D_BG_InitStruct.DMA2D_BGMA = vbuf + offset;
  DMA2D_BG_InitStruct.DMA2D_BGCM = CM_RGB565;
  DMA2D_BG_InitStruct.DMA2D_BGPFC_ALPHA_MODE = NO_MODIF_ALPHA_VALUE;
  DMA2D_BG_InitStruct.DMA2D_BGO = LCD_MAXX - w;
  DMA2D_BGConfig(&DMA2D_BG_InitStruct);

  DMA2D_StartTransfer();

  while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET);
}

void painter_draw_hline_clut(uint32_t x, uint32_t y, uint32_t w)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  DMA2D_InitTypeDef DMA2D_InitStruct;
  DMA2D_DeInit();
  DMA2D_StructInit(&DMA2D_InitStruct);
  DMA2D_InitStruct.DMA2D_Mode = DMA2D_M2M_BLEND;
  DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
  DMA2D_InitStruct.DMA2D_OutputMemoryAdd = vbuf + offset;
  DMA2D_InitStruct.DMA2D_OutputGreen = 0;
  DMA2D_InitStruct.DMA2D_OutputBlue = 0;
  DMA2D_InitStruct.DMA2D_OutputRed = 0;
  DMA2D_InitStruct.DMA2D_OutputAlpha = 0;
  DMA2D_InitStruct.DMA2D_OutputOffset = 0;
  DMA2D_InitStruct.DMA2D_NumberOfLine = 1;
  DMA2D_InitStruct.DMA2D_PixelPerLine = w;
  DMA2D_Init(&DMA2D_InitStruct);

  DMA2D_FG_InitTypeDef DMA2D_FG_InitStruct;
  DMA2D_FG_StructInit(&DMA2D_FG_InitStruct);
  DMA2D_FG_InitStruct.DMA2D_FGMA = vbuf_clut;
  DMA2D_FG_InitStruct.DMA2D_FGCM = CM_L8;
  DMA2D_FGConfig(&DMA2D_FG_InitStruct);

  DMA2D_BG_InitTypeDef DMA2D_BG_InitStruct;
  DMA2D_BG_StructInit(&DMA2D_BG_InitStruct);
  DMA2D_BG_InitStruct.DMA2D_BGMA = vbuf + offset;
  DMA2D_BG_InitStruct.DMA2D_BGCM = CM_RGB565;
  DMA2D_BG_InitStruct.DMA2D_BGPFC_ALPHA_MODE = NO_MODIF_ALPHA_VALUE;
  DMA2D_BGConfig(&DMA2D_BG_InitStruct);

  DMA2D_StartTransfer();

  while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET);
}

void painter_draw_vline_clut(uint32_t x, uint32_t y, uint32_t h)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  DMA2D_InitTypeDef DMA2D_InitStruct;
  DMA2D_DeInit();
  DMA2D_StructInit(&DMA2D_InitStruct);
  DMA2D_InitStruct.DMA2D_Mode = DMA2D_M2M_BLEND;
  DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
  DMA2D_InitStruct.DMA2D_OutputMemoryAdd = vbuf + offset;
  DMA2D_InitStruct.DMA2D_OutputGreen = 0;
  DMA2D_InitStruct.DMA2D_OutputBlue = 0;
  DMA2D_InitStruct.DMA2D_OutputRed = 0;
  DMA2D_InitStruct.DMA2D_OutputAlpha = 0;
  DMA2D_InitStruct.DMA2D_OutputOffset = LCD_MAXX - 1;
  DMA2D_InitStruct.DMA2D_NumberOfLine = h;
  DMA2D_InitStruct.DMA2D_PixelPerLine = 1;
  DMA2D_Init(&DMA2D_InitStruct);

  DMA2D_FG_InitTypeDef DMA2D_FG_InitStruct;
  DMA2D_FG_StructInit(&DMA2D_FG_InitStruct);
  DMA2D_FG_InitStruct.DMA2D_FGMA = vbuf_clut;
  DMA2D_FG_InitStruct.DMA2D_FGCM = CM_L8;
  DMA2D_FGConfig(&DMA2D_FG_InitStruct);

  DMA2D_BG_InitTypeDef DMA2D_BG_InitStruct;
  DMA2D_BG_StructInit(&DMA2D_BG_InitStruct);
  DMA2D_BG_InitStruct.DMA2D_BGMA = vbuf + offset;
  DMA2D_BG_InitStruct.DMA2D_BGCM = CM_RGB565;
  DMA2D_BG_InitStruct.DMA2D_BGPFC_ALPHA_MODE = NO_MODIF_ALPHA_VALUE;
  DMA2D_BG_InitStruct.DMA2D_BGO = LCD_MAXX - 1;
  DMA2D_BGConfig(&DMA2D_BG_InitStruct);


  DMA2D_StartTransfer();

  while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET);
}

void painter_point(uint32_t x, uint32_t y)
{
  x += sx;
  y += sy;
  if(x < vbuf_minx || x >= vbuf_maxx || y < vbuf_miny || y >= vbuf_maxy) return;

  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  DMA2D_InitTypeDef DMA2D_InitStruct;
  DMA2D_DeInit();
  DMA2D_StructInit(&DMA2D_InitStruct);
  DMA2D_InitStruct.DMA2D_Mode = DMA2D_M2M_BLEND;
  DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
  DMA2D_InitStruct.DMA2D_OutputMemoryAdd = vbuf + offset;
  DMA2D_InitStruct.DMA2D_OutputGreen = 0;
  DMA2D_InitStruct.DMA2D_OutputBlue = 0;
  DMA2D_InitStruct.DMA2D_OutputRed = 0;
  DMA2D_InitStruct.DMA2D_OutputAlpha = 0;
  DMA2D_InitStruct.DMA2D_OutputOffset = 0;
  DMA2D_InitStruct.DMA2D_NumberOfLine = 1;
  DMA2D_InitStruct.DMA2D_PixelPerLine = 1;
  DMA2D_Init(&DMA2D_InitStruct);

  DMA2D_FG_InitTypeDef DMA2D_FG_InitStruct;
  DMA2D_FG_StructInit(&DMA2D_FG_InitStruct);
  DMA2D_FG_InitStruct.DMA2D_FGMA = vbuf_clut;
  DMA2D_FG_InitStruct.DMA2D_FGCM = CM_L8;
  DMA2D_FGConfig(&DMA2D_FG_InitStruct);

  DMA2D_BG_InitTypeDef DMA2D_BG_InitStruct;
  DMA2D_BG_StructInit(&DMA2D_BG_InitStruct);
  DMA2D_BG_InitStruct.DMA2D_BGMA = vbuf + offset;
  DMA2D_BG_InitStruct.DMA2D_BGCM = CM_RGB565;
  DMA2D_BG_InitStruct.DMA2D_BGPFC_ALPHA_MODE = NO_MODIF_ALPHA_VALUE;
  DMA2D_BGConfig(&DMA2D_BG_InitStruct);

  DMA2D_StartTransfer();

  while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET);
}

#define swap(a, b) { int32_t t = a; a = b; b = t; }

//void painter_line(int32_t x0, int32_t y0, int32_t x1, int32_t y1)
//{
//    int32_t x, y, x2, y2, dx, dy, err;
//    int32_t xinc, yinc;
//
//    x2 = x1;
//    y2 = y1;
//    x = x0;
//    y = y0;
//    dx = x2 - x;
//    dy = y2 - y;
//
//    if(dx < 0)
//    {
//        dx = -dx;
//        xinc = -1;
//    }
//    else xinc = 1;
//
//    if(dy < 0)
//    {
//        dy = -dy;
//        yinc = -1;
//    }
//    else yinc = 1;
//
////    if(dx > dy)
////    {
////      swap(dy, dx);
////      swap(x, y);
////      swap(xinc, yinc);
////    }
//    if(dx > dy)
//    {
//        dy <<= 1;
//        err = dy - dx;
//        dx = dy - (dx * 2);
//
//        while(x != x2)
//        {
//          painter_point(x, y);
//
//            if(err > 0)
//            {
//                y += yinc;
//                err += dx;
//            }
//            else err += dy;
//            x += xinc;
//        }
//    }
//    else
//    {
//        dx <<= 1;
//        err = dx - dy;
//        dy = dx - (dy * 2);
//
//        while(y != y2)
//        {
//          painter_point(x, y);
//
//            if(err > 0)
//            {
//                x += xinc;
//                err += dy;
//            }
//            else err += dx;
//            y += yinc;
//        }
//    }
//}


void painter_line(int32_t x0, int32_t y0, int32_t x1, int32_t y1)
{
  int32_t dy = y1 - y0;
  int32_t dx = x1 - x0;
  int32_t steep = abs(dy) > abs(dx);

  if(steep)
  {
    swap(x0, y0);
    swap(x1, y1);
    swap(dx, dy);
  }

  dy = abs(dy);

  if(dx < 0)
  {
    swap(x0, x1);
    swap(y0, y1);
    dx = -dx;
  }

  int32_t err = dx >> 1;
  int32_t ystep;

  if(y0 < y1) ystep = 1;
  else ystep = -1;

  do
  {
    if(steep) painter_point(y0, x0);
    else painter_point(x0, y0);

    err -= dy;
    if(err < 0)
    {
      y0 += ystep;
      err += dx;
    }
  } while(x0++ < x1);
}

void painter_fill_triangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2)
{
  int32_t a, b, y, last;

  // Sort coordinates by Y order (y2 >= y1 >= y0)
  if (y0 > y1) {
    swap(y0, y1); swap(x0, x1);
  }
  if (y1 > y2) {
    swap(y2, y1); swap(x2, x1);
  }
  if (y0 > y1) {
    swap(y0, y1); swap(x0, x1);
  }

  if (y0 == y2) { // Handle awkward all-on-same-line case as its own thing
    a = b = x0;
    if(x1 < a)      a = x1;
    else if(x1 > b) b = x1;
    if(x2 < a)      a = x2;
    else if(x2 > b) b = x2;
    painter_draw_fast_hline(a, y0, b-a+1);
    return;
  }

  int32_t
  dx01 = x1 - x0,
  dy01 = y1 - y0,
  dx02 = x2 - x0,
  dy02 = y2 - y0,
  dx12 = x2 - x1,
  dy12 = y2 - y1,
  sa   = 0,
  sb   = 0;

  // For upper part of triangle, find scanline crossings for segments
  // 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
  // is included here (and second loop will be skipped, avoiding a /0
  // error there), otherwise scanline y1 is skipped here and handled
  // in the second loop...which also avoids a /0 error here if y0=y1
  // (flat-topped triangle).
  if (y1 == y2) last = y1;   // Include y1 scanline
  else         last = y1-1; // Skip it

  for (y=y0; y<=last; y++) {
    a   = x0 + sa / dy01;
    b   = x0 + sb / dy02;
    sa += dx01;
    sb += dx02;
    /* longhand:
    a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
    b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
    */
    if (a > b) swap(a,b);
    painter_draw_fast_hline(a+1, y, b-a);
  }

  // For lower part of triangle, find scanline crossings for segments
  // 0-2 and 1-2.  This loop is skipped if y1=y2.
  sa = dx12 * (y - y1);
  sb = dx02 * (y - y0);
  for (; y<=y2; y++) {
    a   = x1 + sa / dy12;
    b   = x0 + sb / dy02;
    sa += dx12;
    sb += dx02;
    /* longhand:
    a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
    b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
    */
    if (a > b) swap(a,b);
    painter_draw_fast_hline(a+1, y, b-a);
  }
}

#define MIN(a, b) ((a < b) ? (a) : (b))
#define MAX(a, b) ((a > b) ? (a) : (b))

void painter_draw_fast_hline(int32_t x, int32_t y, int32_t w)
{
  int32_t x2 = x + w;
  x = MAX(x, vbuf_minx);
  x2 = MIN(x2, vbuf_maxx);
  w = x2 - x;
  if(y < vbuf_miny || y >= vbuf_maxy || w < 1) return;

  painter_draw_hline_clut(x, y, w);
}

void painter_draw_fast_vline(int32_t x, int32_t y, int32_t h)
{
  int32_t y2 = y + h;
  y = MAX(y, vbuf_miny);
  y2 = MIN(y2, vbuf_maxy);
  h = y2 - y;
  if(x < vbuf_minx || x >= vbuf_maxx || h < 1) return;

  painter_draw_vline_clut(x, y, h);
}

void painter_draw_rect(int32_t x, int32_t y, uint32_t w, uint32_t h)
{
  x += sx;
  y += sy;

  painter_draw_fast_hline(x, y, w);
  painter_draw_fast_vline(x + w, y, h);
  painter_draw_fast_hline(x + 1, y + h, w);
  painter_draw_fast_vline(x, y + 1, h);
}

void painter_fill_rect(int32_t x, int32_t y, int32_t w, int32_t h)
{
  x += sx;
  y += sy;
  //  painter_fast_fill_rect_dma(vbuf_blending, x, y, w, h, c);
  //  painter_copy_layer_rect(vbuf, vbuf_blending, x, y, w, h);

  int32_t y2 = y + h;
  y = MAX(y, vbuf_miny);
  y2 = MIN(y2, vbuf_maxy);
  h = y2 - y;

  int32_t x2 = x + w;
  x = MAX(x, vbuf_minx);
  x2 = MIN(x2, vbuf_maxx);
  w = x2 - x;

  if(h < 1 || w < 1) return;
  painter_fill_rect_dma_clut(x, y, w, h);
}

void painter_rect(int32_t x, int32_t y, int32_t w, int32_t h)
{
  painter_set_color(0x9fffffff);
  painter_draw_rect(x++, y++, w, h);
  w -= 2;
  h -= 2;
  painter_set_color(0x9f7f1f1f);
  painter_fill_rect(x, y, w, h);
}

void painter_clear()
{
  painter_window(0, 0, LCD_MAXX, LCD_MAXY);
  painter_offset_scr(0, 0);
  painter_fast_fill_rect_dma(vbuf, 0, 0, LCD_MAXX, LCD_MAXY, 0xff000000);
  painter_fast_fill_rect_dma(vbuf + LCD_FRAME_OFFSET, 0, 0, LCD_MAXX, LCD_MAXY, 0);
}

void painter_set_color(uint32_t c)
{
  DMA2D->FGCLUT[0] = c;
}

void painter_draw_circle(int32_t x, int32_t y, uint32_t r)
{
  int32_t f = 1 - r;
  int32_t ddF_x = 1;
  int32_t ddF_y = -2 * r;
  int32_t x1 = 0;
  int32_t y1 = r;

  painter_point(x, y + r);
  painter_point(x, y - r);
  painter_point(x + r, y);
  painter_point(x - r, y);

  while(x1 < y1)
  {
    if(f >= 0)
    {
      y1--;
      ddF_y += 2;
      f += ddF_y;
    }
    x1++;
    ddF_x += 2;
    f += ddF_x;

    painter_point(x + x1, y + y1);
    painter_point(x - x1, y + y1);
    painter_point(x + x1, y - y1);
    painter_point(x - x1, y - y1);

    painter_point(x + y1, y + x1);
    painter_point(x - y1, y + x1);
    painter_point(x + y1, y - x1);
    painter_point(x - y1, y - x1);
  }
}

void painter_fill_circle(int32_t x, int32_t y, uint32_t r)
{
  int32_t f = 1 - r;
  int32_t ddF_x = 1;
  int32_t ddF_y = -2 * r;
  int32_t x1 = 0;
  int32_t y1 = r;

  painter_draw_fast_vline(x, y + r, 1);
  painter_draw_fast_vline(x, y - r, 1);
  painter_draw_fast_vline(x + r, y, 1);
  painter_draw_fast_vline(x - r, y, 1);
  painter_draw_fast_vline(x - r, y, 2 * r);

  while(x1 < y1)
  {
    if(f >= 0)
    {
      y1--;
      ddF_y += 2;
      f += ddF_y;
    }
    x1++;
    ddF_x += 2;
    f += ddF_x;

    painter_draw_fast_vline(x - x1, y + y1, 2 * x1);
    painter_draw_fast_vline(x - x1, y - y1, 2 * x1);

    painter_draw_fast_vline(x - y1, y + x1, 2 * y1);
    painter_draw_fast_vline(x - y1, y - x1, 2 * y1);
  }
}
