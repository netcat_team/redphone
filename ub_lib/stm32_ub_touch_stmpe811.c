
#include "stm32_ub_touch_stmpe811.h"

Touch_Data_t Touch_Data;

#define I2C_BME280 0xEC
#define I2C_EEPROM 0xA0
#define I2C_CTS 0x70
#define I2C_OV 0x60
#define I2C_GYRO 0xD4
#define I2C_AXEL 0x32
#define I2C_AUDIO 0x34

void delay_cyc3(volatile unsigned int cnt) { while(cnt--); }

Touch_Data_t *ts_get_data()
{
  return &Touch_Data;
}

void ts_update()
{
//  uint32_t xDiff, yDiff;
  uint8_t x;
//  uint8_t y;
//  static uint32_t _x = 0, _y = 0;
  
  i2c2_read(I2C_BME280, 0x88, 1, &x, 1);
  i2c2_read(I2C_EEPROM, 0x00, 1, &x, 1);
  i2c2_read(I2C_CTS, 0x00, 1, &x, 1);
//  i2c2_read(I2C_OV, 0xff, 1, &x, 1);
  
//  if(i2c_wert) Touch_Data.status = TOUCH_PRESSED;
//  else Touch_Data.status = TOUCH_RELEASED;
//
//  if(Touch_Data.status==TOUCH_PRESSED)
//  {
//    y = P_Touch_Read_Y();
//    x = P_Touch_Read_X();
//
//    xDiff = x > _x? (x - _x): (_x - x);
//    yDiff = y > _y? (y - _y): (_y - y);
//    if (xDiff + yDiff > 5)
//    {
//      _x = x;
//      _y = y;
//    }
//  }
//
//  Touch_Data.xp = _x;
//  Touch_Data.yp = _y;
}
