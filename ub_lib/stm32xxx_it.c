/**
  ******************************************************************************
  * @file    stm32xxx_it.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    20-September-2013
  * @brief   Exceptions Handlers
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

//--------------------------------------------------------------
// Globale Variabeln
//--------------------------------------------------------------

//--------------------------------------------------------------
// ISR-Funktionen
//--------------------------------------------------------------
extern void LTDC_ISR_Handler(void);
extern void DMA2D_ISR_Handler(void);

//--------------------------------------------------------------
void NMI_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void HardFault_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void MemManage_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void BusFault_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void UsageFault_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void DebugMon_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void SVC_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void PendSV_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void SysTick_Handler(void)
{
  while(1);
}

//--------------------------------------------------------------
void LTDC_IRQHandler(void)
{
  //LTDC_ISR_Handler();
}

//--------------------------------------------------------------
void DMA2D_IRQHandler(void)
{
  //DMA2D_ISR_Handler();
}


