#ifndef __STM32F4_UB_TOUCH_STMPE811_H
#define __STM32F4_UB_TOUCH_STMPE811_H

#include "stm32f4xx.h"
#include "stm32_ub_i2c3.h"

#define DEVICE_MODE 0x00
#define GEST_ID 0x01
#define TD_STATUS 0x02

#define TOUCH1_XH 0x03
#define TOUCH1_XL 0x04
#define TOUCH1_YH 0x05
#define TOUCH1_YL 0x06

#define TOUCH2_XH 0x09
#define TOUCH2_XL 0x0A
#define TOUCH2_YH 0x0B
#define TOUCH2_YL 0x0C

#define TOUCH3_XH 0x0F
#define TOUCH3_XL 0x10
#define TOUCH3_YH 0x11
#define TOUCH3_YL 0x12

typedef enum
{
  TOUCH_PRESSED  = 0,
  TOUCH_RELEASED = 1
} Touch_Status_t;

typedef struct
{
  Touch_Status_t status;
  uint16_t xp;
  uint16_t yp;
}Touch_Data_t;

Touch_Data_t *ts_get_data();
void ts_update();

#endif // __STM32F4_UB_TOUCH_STMPE811_H
