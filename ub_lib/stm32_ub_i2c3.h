//--------------------------------------------------------------
// File     : stm32_ub_i2c3.h
//--------------------------------------------------------------

//--------------------------------------------------------------
#ifndef __STM32F4_UB_I2C2_H
#define __STM32F4_UB_I2C2_H


#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_i2c.h"

#define I2C2_CLOCK_FRQ   100000  // I2C-Frq in Hz (100 kHz)
#define i2c2_timeout(x) timeout_cnt = 0xFFFF; while (x) { if (timeout_cnt-- == 0) goto errReturn; }

void i2c2_init();
uint8_t i2c2_read(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len);

//--------------------------------------------------------------
#endif // __STM32F4_UB_I2C2_H
