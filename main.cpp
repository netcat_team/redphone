
extern "C" {
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_sdio.h"
#include "stm32_ub_i2c3.h"
#include "ub_lib/stm32_ub_touch_stmpe811.h"
#include "painter.h"
#include "bsp/rm68120.h"
#include "hal/hw_fmc.h"
#include "hal/hw_gpio.h"
#include "hal/hw_sdio.h"
#include "hal/hw_rcc.h"
//#include "bsp/img.h"
}

#include "xlabel.h"
#include "xwindow.h"
#include "xpushbutton.h"
#include "gxlib/widgets/xswitch.h"
#include "xslider.h"
#include "xtetrix.h"
#include "xsystem.h"

void delay_cyc2(volatile unsigned int cnt) { while(cnt--); }

static void sdram_test_w16b(uint32_t adr, uint16_t wert)
{ *(uint16_t*)(SDRAM_START_ADR + adr) = wert; }

static uint16_t sdram_test_r16b(uint32_t adr)
{ return *(volatile uint16_t*)(SDRAM_START_ADR + adr); }

static uint32_t sdram_test()
{
  uint16_t oldwert = sdram_test_r16b(0x00);
  sdram_test_w16b(0x00, 0x5A3C);
  uint16_t istwert = sdram_test_r16b(0x00);
  sdram_test_w16b(0x00, oldwert);

  return istwert == 0x5A3C;
}

int hw_init()
{
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN |
                  RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOGEN |
                  RCC_AHB1ENR_DMA2DEN | RCC_AHB1ENR_DMA2EN;

  // leds
  gpio_init(GPIOB, 9, GPIO_MODE_OUT);
  // shdn
  gpio_init(GPIOG, 10, GPIO_MODE_OUT);
  // but3
  gpio_init(GPIOC, 4, GPIO_MODE_IN);
  // fpga_en
  gpio_init(GPIOB, 12, GPIO_MODE_OUT);
  // gsm_en
  gpio_init(GPIOG, 6, GPIO_MODE_OUT);

  // en bl
  GPIO_SetBits(GPIOA, GPIO_Pin_4);
  // led en
  GPIO_SetBits(GPIOB, GPIO_Pin_9);
  // sys en
  GPIO_SetBits(GPIOG, GPIO_Pin_10);
  // fpga en
  GPIO_SetBits(GPIOB, GPIO_Pin_12);
  // gsm en
  GPIO_ResetBits(GPIOG, GPIO_Pin_6);

  fmc_init();
  fmc_sram_init();
  fmc_sdram_init();

  lcd_init();

//  i2c2_init();
//  sdio_init();
//  painter_init();

  if(sdram_test()) GPIO_SetBits(GPIOB, GPIO_Pin_9);
  else GPIO_ResetBits(GPIOB, GPIO_Pin_9);

  return 0;
}

int main()
{
  set_sys_clock();
  hw_init();

  uint32_t i = 0;

  while(1)
  {
    lcd_set_addr0();
    for(i = 0; i < LCD_SIZE; i++) LCD_WRITE_DATA(0xf00f);
    lcd_set_addr0();
    for(i = 0; i < LCD_SIZE; i++) LCD_WRITE_DATA(0x00ff);
  }

  return 0;
}

