NAME = gx
MCU = cortex-m4
TOOLCHAIN = arm-none-eabi

CC = $(TOOLCHAIN)-gcc
CPPC = $(TOOLCHAIN)-g++
OBJCOPY = $(TOOLCHAIN)-objcopy
GDB = $(TOOLCHAIN)-gdb
SIZE = $(TOOLCHAIN)-size

# ld
LDFILE = link.ld

# src
SRC += ./painter.c

SRC += ./cmsis/startup_stm32f4xx.c
SRC += ./cmsis/system_stm32f4xx.c

SRC += ./spl/stm32f4xx_dma.c
SRC += ./spl/stm32f4xx_dma2d.c
SRC += ./spl/stm32f4xx_gpio.c
SRC += ./spl/stm32f4xx_i2c.c
SRC += ./spl/stm32f4xx_ltdc.c
SRC += ./spl/stm32f4xx_rcc.c
SRC += ./spl/stm32f4xx_spi.c
SRC += ./spl/stm32f4xx_sdio.c
SRC += ./spl/misc.c

SRC += ./ub_lib/stm32_ub_i2c3.c
SRC += ./ub_lib/stm32_ub_spi5.c
SRC += ./ub_lib/stm32_ub_touch_stmpe811.c
SRC += ./ub_lib/stm32xxx_it.c

SRC += ./bsp/rm68120.c

OBJS = ${SRC:.c=.o}

#cpp src
CPPSRC += ./main.cpp

CPPSRC += ./gxlib/xobject.cpp
CPPSRC += ./gxlib/xpainter.cpp
CPPSRC += ./gxlib/xsystem.cpp
CPPSRC += ./gxlib/xwidget.cpp
CPPSRC += ./gxlib/xstring.cpp
CPPSRC += ./gxlib/xscreen.cpp
CPPSRC += ./gxlib/xtouch.cpp

CPPSRC += ./gxlib/widgets/xlabel.cpp
CPPSRC += ./gxlib/widgets/xwindow.cpp
CPPSRC += ./gxlib/widgets/xpushbutton.cpp
CPPSRC += ./gxlib/widgets/xtetrix.cpp
CPPSRC += ./gxlib/widgets/xswitch.cpp
CPPSRC += ./gxlib/widgets/xslider.cpp

SRC += ./hal/hw_gpio.c
SRC += ./hal/hw_fmc.c
SRC += ./hal/hw_sdio.c
SRC += ./hal/hw_rcc.c

CPPOBJS = ${CPPSRC:.cpp=.o}

#inc
INC += .
INC += ./cmsis
INC += ./spl
INC += ./ub_lib
INC += ./bsp

#cpp inc
INC += ./gxlib
INC += ./gxlib/widgets

CINC = $(INC:%=-I%)

CFLAGS = -mcpu=$(MCU) -mthumb -g -Wall -O2 -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -DSTM32F429_439xx -DUSE_STDPERIPH_DRIVER -ffunction-sections -fdata-sections
CPPFLAGS = $(CFLAGS) -fno-rtti -fno-exceptions -std=c++11
LDFLAGS = -mcpu=$(MCU) -mthumb -T${LDFILE} --specs=nosys.specs -nostartfiles -Wl,--gc-sections -flto
#-Wl,-Map=$(NAME).map

all: $(OBJS) $(CPPOBJS) $(NAME).elf $(NAME).bin

.c.o:
	$(CC) $(CFLAGS) $(CINC) -c $< -o $@

.cpp.o:
	$(CPPC) $(CPPFLAGS) $(CINC) -c $< -o $@

$(NAME).elf: $(OBJS)
	$(CPPC) $(LDFLAGS) $(OBJS) $(CPPOBJS) -o $@

$(NAME).bin: $(NAME).elf
	$(OBJCOPY) -O binary -S $^ $@
	$(SIZE) $(NAME).elf

flash:
	openocd -f openocd.cfg

dbg:
	openocd -f openocd.dbg.cfg

gdb:
	$(GDB) --eval-command="target remote :3333" -tui $(NAME).elf

clean:
	rm -f $(OBJS)
	rm -f $(CPPOBJS)
	rm -f $(NAME).hex
	rm -f $(NAME).elf
	rm -f $(NAME).bin
	rm -f $(NAME).map
