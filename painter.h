/*
 * painter.h
 *
 *  Created on: Jan 4, 2016
 *      Author: netcat
 */

#ifndef PAINTER_H_
#define PAINTER_H_

#define SDRAM_START_ADR ((uint32_t)0xC0000000)

#define LCD_FRAME_BUFFER SDRAM_START_ADR
#define LCD_COLOR_SIZE ((uint32_t)2)
#define LCD_FRAME_OFFSET ((uint32_t)(LCD_SIZE * LCD_COLOR_SIZE))

void painter_init();
void painter_copy_rect(uint32_t x, uint32_t y, uint32_t w, uint32_t h);

void painter_fill_rect(int32_t x, int32_t y, int32_t w, int32_t h);
void painter_set_color(uint32_t c);
void painter_clear();
void painter_draw_fast_vline(int32_t x, int32_t y, int32_t h);
void painter_draw_fast_hline(int32_t x, int32_t y, int32_t w);
void painter_draw_rect(int32_t x, int32_t y, uint32_t w, uint32_t h);
void painter_rect(int32_t x, int32_t y, int32_t w, int32_t h);
void painter_point(uint32_t x, uint32_t y);
void painter_line(int32_t x0, int32_t y0, int32_t x1, int32_t y1);
void painter_fill_triangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2);

void painter_offset_scr(int32_t x, int32_t y);
void painter_window(uint32_t x, uint32_t y, uint32_t w, uint32_t h);

void painter_draw_circle(int32_t x, int32_t y, uint32_t r);
void painter_fill_circle(int32_t x, int32_t y, uint32_t r);

void painter_copy_layer_rect(uint32_t vbuf_to, uint32_t vbuf_from, uint32_t x, uint32_t y, uint32_t w, uint32_t h);

#endif /* PAINTER_H_ */
